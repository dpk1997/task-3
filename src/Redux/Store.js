import { createStore } from "redux";
const intialState = {
  Courses: [
    {
      Title: "Clean ",
      Length: "2:19",
      Category: "Software Practices",
      Auther: "cory-house"
    },
    {
      Title: "Clean 1",
      Length: "2:19",
      Category: "Software Practices",
      Auther: "cory-house"
    },
    {
      Title: "Clean2 ",
      Length: "2:19",
      Category: "Software Practices",
      Auther: "cory-house"
    }
  ],
  selectedRow: null
};

const reducer = (state = intialState, action) => {
  switch (action.type) {
    case "Add":
      const Courses = {
        Title: action.Title,
        Length: action.Length,
        Category: action.Category,
        Auther: action.Auther
      };
      return {
        ...state,
        Courses: [...state.Courses, Courses]
      };
    case "Selected Row":
      const index = action.selected;
      return {
        ...state,
        selectedRow: index
      };

    case "Edit":
      const  course=[...state.Courses]
        course[action.index]={...course,
                Title: action.Title,
                Length: action.Length,
                Category: action.Category,
                Auther: action.Auther}
      return{
          ...state,
          Courses: course,
          selectedRow:null
      }

      case "Delete":
       const totalCourse=[...state.Courses];
       const filter= totalCourse.filter((item,index)=>{
            if(index===action.index)
            return false;

            return true;
        })
      return{
          ...state,
          Courses:filter,
          selectedRow:null
      }


    default:
      return state;
  }
};

const store = createStore(reducer);

export default store;
