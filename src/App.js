import React from "react";
import { BrowserRouter, Route} from "react-router-dom";

import Courses from './Courses/Courses'
import Course from './Courses/Course/Course'
import EditCourse from './Courses/Edit/EditCourse'


import "./App.css";

function App() {
  return (
    <BrowserRouter>
      <div>
        <nav className="navbar navbar-toggleable-sm bg-info navbar-inverse">
          <div className="container">
            <div className="navbar nav">
              <a href="/home" className="nav-item nav-link">Home</a>
              <a href="/"  className="nav-item nav-link active">Course</a>
              <a href="/about" className="nav-item nav-link">About</a>
            </div>
          </div>
        </nav>

        
        <Route path='/' exact component={Courses}/>
        <Route path='/course' component={Course} />
        <Route path='/edit' component={EditCourse}/>
      
      </div>
    </BrowserRouter>
  );
}

export default App;
