import React from "react";
import './Courses.css'
import {Redirect,Link} from "react-router-dom";
import { connect } from "react-redux";
import { isNull } from "util";



class courses extends React.Component{
    state={
        clicked:false,
        index:null
    }

 indexHandler=(index)=>
{
    this.props.dispatch(
        {
            type:'Selected Row',
            selected:index
        }
    )
    this.setState((preState)=>{
        if(preState.index===index)
        return{
            clicked:!this.state.clicked,
        }
       else
       return{
        clicked:true,
        index:index
       }
      
    })


}

deleteHandler=(event)=>{
    event.preventDefault();
    this.props.dispatch({
        type:'Delete',
        index:this.state.index

    })
    this.setState({
        index:null
    })
   return(
             <Redirect to='/'/>
   );
    
}



 render(){
     
  const totalCourses = this.props.courses.map((course, index) => {
 return (
   <tr  key={index}  onClick={()=>this.indexHandler(index)} style={{backgroundColor:(this.state.index===index&& this.state.clicked===true)?'rgb(193, 242, 145)':''}}>
     <td>{course.Title}</td>
     <td>{course.Length}</td>
     <td>{course.Category}</td>
     <td>{course.Auther}</td>
   </tr>
 );
});
    return (
        <div className="container-fluid">
          <div className="row mt-3">
            <div className="col">
              <h1>Courses</h1>
            </div>
          </div>
    
          <div className="row mt-3">
            <div className="col">
              <Link to="/course"  className="btn btn-primary">
                New
              </Link>
              <Link to={isNull(this.props.selected)?"/":"/edit"} className="btn btn-warning ml-2">
                Edit
              </Link>
              <Link to="/" className="btn btn-danger ml-2" onClick={this.deleteHandler}>
                Delete
              </Link>
            </div>
          </div>
    
          <div className="row mt-3">
            <div className="col">
              <table className="table table-hover table-striped table-condensed">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Length</th>
                    <th>Category</th>
                    <th>Auther</th>
                  </tr>
                </thead>
                <tbody>{totalCourses}</tbody>
              </table>
            </div>
          </div>
        </div>
      );

 }
  
};

const mapStateToProps = state => ({
    
  courses: state.Courses,
  selected:state.selectedRow
});

export default connect(mapStateToProps, null)(courses);
