import React from "react";
import {connect} from 'react-redux';
class course extends React.Component {

    state={ 
        Title:'',
        Auther:'',
        Length:'',
        Category:''
    };

    submitHandler=(event)=>
    {
        event.preventDefault();
        this.props.dispatch({
            type:'Add',
            Title:this.state.Title,
            Auther:this.state.Auther,
            Category:this.state.Category,
            Length:this.state.Length
            
        });

        this.props.history.replace('/');
    
    }

    change=(event)=>
    {
      let input=  event.target.name;
        this.setState({
            [input]:event.target.value
        })
    }

    cancelHandler=(event)=>{
        event.preventDefault();
      this.props.history.push('/');
    }

    clearHandler=(event)=>
    {
        event.preventDefault();
        this.setState({
            Title:'',
            Auther:'',
            Length:'',
            Category:''
        })

    }
    render()
    { 
  return (
       
    <div className="container">
      <form>
        <h1>Add</h1>

        <div className="form-group">
          <label>Title</label>
          <div className="field">
            <input
              type="text"
              name='Title'
              value={this.state.Title}
              className="form-control"
              placeholder="Title of the course"
            onChange={this.change} 
            />
          </div>
        </div>

        <div className="form-group">
          <div>Author</div>
          <div className="field">
            <select className="form-control" name='Auther'  onChange={this.change} value={this.state.Auther}>
              <option></option>
              <option value="cory-house">Cory House</option>
              <option value="scott-allen">Scott Allen</option>
              <option value="dan-wahlin">Dan Wahlin</option>
            </select>
          </div>
        </div>

        <div className="form-group">
          <label>Category</label>
          <div className="field">
            <input
              type="text"
              name='Category'
              value={this.state.Category}
              className="form-control"
              placeholder="Category of the course"
              onChange={this.change}
              
            />
          </div>
        </div>

        <div className="form-group">
          <label>Length</label>
          <div className="field">
            <input
              type="text"
              name='Length'
              value={this.state.Length}
              className="form-control"
              placeholder="Length of the course in minutes or hours"
              onChange={this.change}
              
            />
          </div>
        </div>


        <button className='btn btn-primary' onClick={this.submitHandler}>Submit</button>
        <button className='btn btn-secondary ml-2' onClick={this.clearHandler} >Clear Values</button>
        <button className='btn btn-secondary ml-2' onClick={this.cancelHandler}>Cancel</button>
      </form>
    </div>
  );

    }

};

export default connect(null,null)(course);
