import React from "react";
import { connect } from "react-redux";

class editCourse extends React.Component {
  state = {
    course: {
      Title: "",
      Length: "",
      Category: "",
      Auther: ""
    }
  };
  componentDidMount = () => {
    this.setState(
      {
        course: this.props.course
      }
    );
  };
  changeHandler = event => {
    event.persist();
    this.setState(preState => ({
      course: { ...preState.course, [event.target.name]: event.target.value }
    }));
  };

  cancelHandler = event => {
    event.preventDefault();
    this.props.history.push("/");
  };

  submitHandler = event => {
    event.preventDefault();
    this.props.dispatch({
      type: "Edit",
      index: this.props.index,
      Title: this.state.course.Title,
      Auther: this.state.course.Auther,
      Category: this.state.course.Category,
      Length: this.state.course.Length
    });

    this.props.history.replace("/");
  };

  render() {
    return (
      <div className="container">
        <form>
          <h1>Add</h1>

          <div className="form-group">
            <label>Title</label>
            <div className="field">
              <input
                type="text"
                name="Title"
                value={this.state.course.Title}
                onChange={this.changeHandler}
                className="form-control"
                placeholder="Title of the course"
              />
            </div>
          </div>

          <div className="form-group">
            <div>Author</div>
            <div className="field">
              <select
                className="form-control"
                name="Auther"
                value={this.state.course.Auther}
                onChange={this.changeHandler}
              >
                <option></option>
                <option value="cory-house">Cory House</option>
                <option value="scott-allen">Scott Allen</option>
                <option value="dan-wahlin">Dan Wahlin</option>
              </select>
            </div>
          </div>

          <div className="form-group">
            <label>Category</label>
            <div className="field">
              <input
                type="text"
                name="Category"
                value={this.state.course.Category}
                className="form-control"
                placeholder="Category of the course"
                onChange={this.changeHandler}
              />
            </div>
          </div>

          <div className="form-group">
            <label>Length</label>
            <div className="field">
              <input
                type="text"
                name="Length"
                value={this.state.course.Length}
                className="form-control"
                placeholder="Length of the course in minutes or hours" 
                onChange={this.changeHandler}
              />
            </div>
          </div>

          <button className="btn btn-primary" onClick={this.submitHandler}>
            {" "}
            Submit{" "}
          </button>
          <button
            className="btn btn-secondary ml-2"
            onClick={this.cancelHandler}
          >
            Cancel
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  index: state.selectedRow,
  course: state.Courses[state.selectedRow]
});
export default connect(mapStateToProps)(editCourse);
